package nl.cupcake.app
import org.scalatra.UrlGeneratorSupport
import play.api.libs.json.{JsValue, Json}

import scala.util.parsing.json.JSONObject

import scalaj.http.{HttpResponse, Http}

class UserController extends SotfwebStack with UrlGeneratorSupport {
  val titleL = "Coop Login"
  val titleR = "Coop Registeren"
  val error = "U bent niet goed ingelogd probeer opnieuw"

  get("/registeren") {
    contentType = "text/html"
    ssp("view_registeren", "title" -> titleR, "background" -> true)
  }
  get("/inloggen") {
    contentType = "text/html"
    ssp("view_login", "title" -> titleL, "background" -> true)
  }

  post("/registeren") {
    // de ingevulde gegevens ophalen
    val email = params.get("email").getOrElse("")
    val ww1 = params.get("wachtwoord").getOrElse("")
    val ww2 = params.get("wachtwoord2").getOrElse("")
    val voornaam = params.get("voornaam").getOrElse("")
    val achternaam = params.get("achternaam").getOrElse("")
    val adres = params.get("adres").getOrElse("")
    val huisnummer = params.get("huisnummer").getOrElse("")
    val postcode = params.get("postcode").getOrElse("")
    val plaats = params.get("plaats").getOrElse("")
    // check of ww1 en ww2 het zelfde is
    if (ww1.equals(ww2)) {
      var map: Map[String, String] = Map()
      map += ("first_name" -> voornaam)
      map += ("last_name" -> achternaam)
      map += ("street" -> adres)
      map += ("house_number" -> huisnummer)
      map += ("addition" -> "")
      map += ("postal_code" -> postcode)
      map += ("city" -> plaats)
      map += ("username" -> email)
      map += ("password" -> ww2)

      // JSONObject maken
      val jsonRequest: JSONObject = new JSONObject(map)
      // url van de API
      val url = "http://snowboom.nu:8123/register"
      // de inloggegevens opsturen
      val responseAPI: HttpResponse[String] = Http(url).postData(jsonRequest.toString()).header("content-type", "application/json").asString
      // kijken of er 200 terug komt
      if (responseAPI.headers.get("status").toString().contains("200")) {
        val feedback = "<a class=\"btn\" onclick=\"Materialize.toast('Bedankt voor het registreren', 4000)\">Toast!</a>"
        // todo nog wat doen met die feedback
        if (inloggen(email, ww2)) {
          val feedback = "<a class=\"btn\" onclick=\"Materialize.toast('U bent nu ingelogd', 4000)\">Toast!</a>"
          // todo nog wat doen met die feedback
          halt(status = 302, headers = Map("Location" -> "/", "Message" -> feedback))
        }
        else{
          println("kon niet inloggen")
        }
      }
      else {
        // resultaat ophalen
        val json: JsValue = Json.parse(responseAPI.body)
        // token en userid er uithalen
        val status = (json \ "status").asOpt[String].getOrElse("")

        if (status.equals("FAILED")) {
          val errorEmail = getLabel("email", (json \ "username").asOpt[String].getOrElse(""))
          val errorVoornaam = getLabel("voornaam", (json \ "first_name").asOpt[String].getOrElse(""))
          val errorAchternaam = getLabel("achternaam", (json \ "last_name").asOpt[String].getOrElse(""))
          val errorAdres = getLabel("adres", (json \ "street").asOpt[String].getOrElse(""))
          val errorHuisnummer = getLabel("huisnummer", (json \ "house_number").asOpt[String].getOrElse(""))
          val errorPostcode = getLabel("postcode", (json \ "postal_code").asOpt[String].getOrElse(""))
          val errorPlaats = getLabel("plaats", (json \ "city").asOpt[String].getOrElse(""))
          val errorw1 = getLabel("wachtwoord", (json \ "password").asOpt[String].getOrElse(""))
          val errorw2 = getLabel("wachtwoord2", (json \ "password").asOpt[String].getOrElse(""))
          contentType = "text/html"
          ssp("view_registeren", "errorw1" -> errorw1, "errorw2" -> errorw2, "errorPlaats" -> errorPlaats, "errorPostcode" -> errorPostcode, "errorHuisnummer" -> errorHuisnummer, "errorAdres" -> errorAdres, "errorAchternaam" -> errorAchternaam, "errorVoornaam" -> errorVoornaam, "errorEmail" -> errorEmail, "title" -> titleR, "email" -> email, "voornaam" -> voornaam, "achternaam" -> achternaam, "adres" -> adres, "huisnummer" -> huisnummer, "postcode" -> postcode, "plaats" -> plaats, "background" -> true)
        }
      }
    }
    else {
      val error = getLabel("wachtwoord", "Wachtwoorden komen niet overeen")
      val error1 = "<label for=\"wachtwoord2\" data-error=\"Wachtwoorden komen niet overeen\" >Typ wachtwoord opnieuw</label>"
      contentType = "text/html"
      ssp("view_registeren", "errorw1" -> error, "errorw2" -> error1, "title" -> titleR, "email" -> email, "voornaam" -> voornaam, "achternaam" -> achternaam, "adres" -> adres, "huisnummer" -> huisnummer, "postcode" -> postcode, "plaats" -> plaats, "background" -> true)
    }
  }

  def getLabel(id: String, error: String): String = {
    var label = ""
    var errorSh = error
    if (error != "") {
      if (error == "FIELD_NOT_VALID") {
        errorSh = "Dit veld is niet goed ingevuld"
      } else if (error == "FIELD_EMPTY") {
        errorSh = "Dit veld mag niet leeg zijn"
      } else if (error == "FIELD_EXISTS") {
        errorSh = "Dit email adres is al ingebruik"
      }

      label = "<label for='" + id + "' data-error='" + errorSh + "' >" + id.capitalize + "</label>"
    }
    return label
  }

  def inloggen(username: String, password: String): Boolean = {
    // de map maken om optesturen
    var map: Map[String, String] = Map()
    map += ("username" -> username)
    map += ("password" -> password)
    // JSONObject maken
    val jsonRequest: JSONObject = new JSONObject(map)
    // url van de API
    val url = "http://snowboom.nu:8123/login"
    // de inloggegevens opsturen
    val responseAPI: HttpResponse[String] = Http(url).postData(jsonRequest.toString()).header("content-type", "application/json").asString
    if (responseAPI.headers.get("status").toString().contains("200")) {
      // resultaat ophalen
      val json: JsValue = Json.parse(responseAPI.body)
      // token en userid er uithalen
      val token = (json \ "token").asOpt[String].getOrElse("")
      val user:Int = (json \ "user").asOpt[Int].getOrElse(0)
      // in session zetten
      cookies.set("token", token)
      cookies.set("user", user.toString())
      return true
    }
    return false
  }

  post("/inloggen") {
    // de ingevulde gegevens ophalen
    val user = params.get("naam").getOrElse("")
    val ww = params.get("wachtwoord").getOrElse("")

    if (inloggen(user, ww)) {
      val feedback = "<a class=\"btn\" onclick=\"Materialize.toast('U bent nu ingelogd', 4000)\">Toast!</a>"
      // todo nog wat doen met die feedback
      halt(status = 302, headers = Map("Location" -> "/", "Message" -> feedback))
    }
    else {
      contentType = "text/html"
      ssp("view_login", "title" -> titleL, "error" -> getLabel("naam", error), "background" -> true)
    }
  }

  get("/uitloggen") {
    cookies.delete("token")
    cookies.delete("user")
    //Cookie.logout()
    halt(status = 302, headers = Map("Location" -> "/"))

  }
}
