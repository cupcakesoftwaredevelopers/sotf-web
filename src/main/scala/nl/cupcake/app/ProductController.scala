package nl.cupcake.app

import scala.collection.mutable.ListBuffer
import nl.cupcake.app.models.Product

/**
  * Created by Martijn on 26-1-2016.
  */
class ProductController extends SotfwebStack {
  get("/") {
    contentType="text/html"
    val title: String = "Coop Assortiment"
    ssp("view_products", "title" -> title)
  }
}
