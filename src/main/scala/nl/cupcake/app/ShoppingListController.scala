package nl.cupcake.app

import play.api.libs.json._

import scala.util.parsing.json.JSONObject
import scalaj.http.{Http, HttpResponse}

/**
  * Created by rvank on 27-1-2016.
  */
class ShoppingListController extends SotfwebStack {
  val title = "Coop boodschappenlijst"
  var output = ""
  get("/") {
    if (cookies.get("token") != null && !cookies.get("token").isEmpty && cookies.get("token").isDefined) {
      // eerst de JSON maken met de gegevens om op te sturen voor de boodschappenlijst
      var map: Map[String, Any] = Map()
      map += ("user" -> cookies.get("user").getOrElse("1").toInt)
      map += ("token" -> cookies.get("token").getOrElse("abc"))
      // JSONObject maken
      val jsonRequest: JSONObject = new JSONObject(map)

      // url van de API
      val url = "http://snowboom.nu:8123/shoppinglist"
      // de inloggegevens opsturen
      val responseAPI: HttpResponse[String] = Http(url).postData(jsonRequest.toString()).header("content-type", "application/json").asString
      // kijken of er 200 terug komt
      if (responseAPI.headers.get("status").toString().contains("200")) {
        // resultaat ophalen
        val json: JsValue = Json.parse(responseAPI.body)

        val producten = (json \ "products").as[List[Products]]
        var totaal: Double = 0.0
        var aantal: Int = 0
        if (producten.length > 0) {
          output = ""
          output += "<form id ='shoppinglistForm' method=\"post\" action=\"/boodschappenlijst\">"
          output += "<table id ='shoppinglistTable' class=\"bordered\">"
          output += "<tr>"
          output += "<th style=\"width:50%\">"
          output += "Product"
          output += "</th>"
          output += "<th style=\"width:16.6%; text-align:right;\" >"
          output += "Aantal"
          output += "</th>"
          output += "<th style=\"width:16.6%; text-align:right;\" >"
          output += "Prijs (€)"
          output += "</th>"
          output += "<th style=\"width:16.6%\">"
          output += "<th>"
          output += "</tr>"
          for (p: Products <- producten) {
            if (p.amount > 0) {
              totaal = totaal + (p.amount * p.price)
              aantal = aantal + p.amount

              var displayTitle: String = p.brand
              if (displayTitle.length > 0) {
                displayTitle += " ";
              }
              displayTitle += p.name

              output += "<tr id =\"" + p.barcode + "\">"
              output += "<td style=\"text-transform: capitalize\">"
              output += displayTitle
              output += "</td>"
              output += "<td id='amount-" + p.barcode + "' style=\"text-align:right;\" >"
              output += p.amount
              output += "</td>"
              output += "<td id='prijs-" + p.barcode + "' style=\"text-align:right;\" >€ "
              output += getalOmzetten(p.price)
              output += "</td>"
              output += "<td>"
              output += createIcons(p.barcode)
              output += "</td>"
              output += "</tr>"
            }
          }
          // todo totaal afronding
          output += "<tr><td style=\"text-align:right;\" >Totaal</td><td id='aantal-totaal' style=\"text-align:right;\" >" + aantal + "</td><td id='prijs-totaal' style=\"text-align:right;\" >€ " + getalOmzetten(BigDecimal(totaal).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble) + "</td><td><input type='submit' class='waves-effect waves-light btn' value='Opslaan'></td></tr>"
          output += "</table>"
          output += "</form>"
        }
        if (aantal == 0) {
          output = "Uw boodschappen lijst is leeg"
        }
      }
      else {
        println(responseAPI.body)
        // todo error terug geven
      }
      contentType = "text/html"
      val saved = params.get("saved") != None
      ssp("view_boodschappen_lijst", "title" -> title, "output" -> output, "saved" -> saved)
    }
    else {
      val feedback = "<a class=\"btn\" onclick=\"Materialize.toast('U moet ingelogd zijn', 4000)\">Toast!</a>"
      // todo nog wat doen met die feedback
      halt(status = 302, headers = Map("Location" -> "/user/inloggen", "Message" -> feedback))
    }
  }

  post("/") {
    for (p <- params) {
      if (p._2.toInt > 0) {
        // map maken met de product spul
        var mapProd: Map[String, Int] = Map()
        mapProd += ("barcode" -> p._1.toInt)
        mapProd += ("amount" -> p._2.toInt)
        // JSONObject maken
        val jsonProd: JSONObject = new JSONObject(mapProd)
        // de map met de standaar gegevens
        var map: Map[String, Any] = Map()
        map += ("user" -> cookies.get("user").getOrElse("").toInt)
        map += ("token" -> cookies.get("token").getOrElse(""))
        map += ("product" -> jsonProd)
        // JSONObject maken
        val jsonRequest: JSONObject = new JSONObject(map)
        // url van de API
        val url = "http://snowboom.nu:8123/shoppinglist/add"
        // de inloggegevens opsturen
        val responseAPI: HttpResponse[String] = Http(url).postData(jsonRequest.toString()).header("content-type", "application/json").asString
        if (responseAPI.headers.get("status").toString().contains("200")) {
          println("succes")
        }
        else {
          println(jsonRequest)
          println(responseAPI.body)
        }
      } else {
        // dit is om het product te verwijderen
        var map: Map[String, Any] = Map()
        map += ("user" -> cookies.get("user").getOrElse("").toInt)
        map += ("token" -> cookies.get("token").getOrElse(""))
        map += ("barcode" -> p._1.toInt)
        // JSONObject maken
        val jsonRequest: JSONObject = new JSONObject(map)
        // url van de API
        val url = "http://snowboom.nu:8123/shoppinglist/delete"
        // de inloggegevens opsturen
        val responseAPI: HttpResponse[String] = Http(url).postData(jsonRequest.toString()).header("content-type", "application/json").asString
        if (responseAPI.headers.get("status").toString().contains("200")) {
          println("succes")
        }
      }
    }
    // todo nog wat leuks terug sturen
    halt(status = 302, headers = Map("Location" -> "/boodschappenlijst?saved=true"))
  }

  def createIcons(id: Long): String = {
    var output = ""
    // plus ico
    output += "<img class='plusArtikelBoodschappen' src=\"/images/plus.png\" />"
    // min ico
    output += "<img class='minArtikelBoodschappen' src=\"/images/minus.png\" />"
    // prullebak ico
    output += "<img class='deleteArtikelBoodschappen' src=\"/images/delete.png\" />"
    return output
  }

  def getalOmzetten(price: Any): Any={
    val symbols = java.text.DecimalFormatSymbols.getInstance();
    symbols.setDecimalSeparator(',');
    symbols.setGroupingSeparator('.');

    val formatter = new java.text.DecimalFormat("#,##0.00", symbols);

    return formatter.format(price);
  }
}

case class Products(barcode: Long, name: String, price: Double, detail: String, brand: String, amount: Int)

object Products {
  implicit val userReads: Reads[Products] = Json.reads[Products]
  implicit val customerWrites: Format[Products] = Json.format[Products]
}
