package nl.cupcake.app

import org.scalatra._
import scalate.ScalateSupport

class HomeController extends SotfwebStack {
  val home = get("/") {
    contentType="text/html"
    val title = "Coop Home"
    ssp("view_home", "body" -> "home",  "title" -> title, "background" -> true, "carts" -> true)
  }
}
