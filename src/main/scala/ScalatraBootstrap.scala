import nl.cupcake.app._
import org.scalatra._
import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new ProductController, "/producten")
    context.mount(new UserController, "/user")
    context.mount(new ShoppingListController, "/boodschappenlijst")
    context.mount(new HomeController, "/")
  }
}
