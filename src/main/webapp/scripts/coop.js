$(document).on('ready', function() {
    var $target = $('.menu-target');

    var loginUrl   = '/user/inloggen';
    var logoutUrl  = '/user/uitloggen';
    var boodschappenUrl  = '/boodschappenlijst';
    var loginText  = 'Inloggen';
    var logoutText = 'Uitloggen';
    var boodschappenText = 'Boodschappenlijst';
    var url = '<li><a href="var:url">var:text</a></li>';
    var listUrl = url.replaceAll('var:text', 'Boodschappenlijst').replaceAll('var:url', '/boodschappenlijst');

    if(Session.getToken()) {
        $target.append(listUrl);
        $target.append(url.replaceAll('var:text', logoutText).replaceAll('var:url', logoutUrl));
    } else {
        $target.append(url.replaceAll('var:text', loginText).replaceAll('var:url', loginUrl));
    }
});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

var API = {
    //URL: 'http://localhost:8123'
    URL: 'http://www.snowboom.nu:8123'
};

var Cookie = {
    getCookie: function(name) {
        var cookie = document.cookie;
        var cookies = cookie.split(';');
        var ret = null;
        cookies.forEach(function(o) {
            var map = o.split('=');
            if(map.length === 2) {
                var key = map[0].trim();
                var val = map[1].trim();
                if(key === name) {
                    ret = parseInt(val);
                    return ret;
                }
            }
        });
        return ret;
    }
};

var Session = {

    getToken: function() {
        var ret = Cookie.getCookie('token');
        if(ret != null) return ret;
    },

    getUserId: function() {
        var ret = Cookie.getCookie('user');
        if(ret != null) return ret;
    }
}

$(document).on("click", ".plusArtikelBoodschappen" ,function() {
    // id ophalen van de rij
    var id = $(this).closest("tr").attr("id");
    // amount van de rij omhoog
    var amount = parseInt($("#amount-"+id).text())+1;
    $("#amount-"+id).html(amount)
    // amount van totaal omhoog
    amount = parseInt($("#aantal-totaal").text())+1;
    $("#aantal-totaal").html(amount)
    $("#prijs-totaal").html("€ "+geldOpnieuwRekenen())
    makeUpdateHiddenForm(id);
});

$(document).on("click",".minArtikelBoodschappen",function() {
    // id ophalen van de rij
    var id = $(this).closest("tr").attr("id");
    // amount van de rij omlaag
    var amount = parseInt($("#amount-"+id).text())-1;
    if (amount>=1){
        $("#amount-"+id).html(amount)
        // amount van totaal omlaag
        amount = parseInt($("#aantal-totaal").text())-1;
        $("#aantal-totaal").html(amount)
        $("#prijs-totaal").html("€ "+geldOpnieuwRekenen())
        makeUpdateHiddenForm(id);
    }
});

$(document).on("click", ".deleteArtikelBoodschappen",function() {
    // id ophalen van de rij
    var id = $(this).closest("tr").attr("id");
    // aantal ophalen
    var amount = parseInt($("#amount-"+id).text());
    amount = parseInt($("#aantal-totaal").text())-amount;
    $("#aantal-totaal").html(amount)
    // amount op 0 zetten zodat die niet word getelt
    $("#amount-"+id).html(0)
    $("#"+id).hide()
    $("#prijs-totaal").html("€ "+geldOpnieuwRekenen())
    makeUpdateHiddenForm(id);
});

function makeUpdateHiddenForm(id){
    var aantalForm = $("#amount-"+id).text();
    if ($("#f"+id).length){
        $("#f"+id).val(aantalForm)
    }
    else{
        $('<input>').attr({
            type: 'hidden',
            id: 'f'+id,
            name: id,
            value: aantalForm
        }).appendTo('form');
        console.log("toegevoegd")
    }
}

function geldOpnieuwRekenen(){
    var totaal =0.0;
    $('#shoppinglistTable > tbody  > tr').each(
        function() {
            var id = $(this).attr('id');
            if (id != null){
                var prijs = $("#prijs-"+id).text()
                prijs = prijs.replaceAll("€", "")
                prijs = prijs.replaceAll(",", ".")
                var amount = parseInt($("#amount-"+id).text())
                var rijTotaal = parseFloat(parseInt($("#amount-"+id).text())*parseFloat(prijs));
                totaal = Math.round((parseFloat(totaal)+parseFloat(rijTotaal)) * 100) / 100;
                totaal = parseFloat(Math.round(totaal * 100) / 100).toFixed(2)
            }
        }
    );
    return totaal;

}